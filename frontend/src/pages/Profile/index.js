import React from "react";
import "./style.css";
import { useAuth } from "../../contexts/auth";

export default function Profile() {
  const { user, signOut } = useAuth();
  console.log(`Usuário: ${user}`);
  function handleSignout() {
    signOut();
  }
  return (
    <div className="profile">
      <h1>Profile</h1>
      <p>{user?.name}</p>
      <button onClick={handleSignout}>Logout</button>
    </div>
  );
}
