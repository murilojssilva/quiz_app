import React from "react";
import Profile from "../pages/Profile";

import { BrowserRouter, Route, Switch } from "react-router-dom";

export default function AuthRoutes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Profile}></Route>
      </Switch>
    </BrowserRouter>
  );
}
